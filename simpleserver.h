#ifndef SIMPLESERVER_H
#define SIMPLESERVER_H

#include <QObject>
#include <QtNetwork/QTcpServer>
#include "../simpleremotecontrol/ClientSocket.h"
#include "../simpleremotecontrol/src_protocol.h"
#include <QtNetwork/QHostAddress>
#include <QDataStream>
#include "commandprocessor.h"
#include <QSet>
#include <QUdpSocket>
#include <QSslPreSharedKeyAuthenticator>
#include <QSslError>

class SimpleServer : public QTcpServer
{
    Q_OBJECT
public:
    explicit SimpleServer(QObject *parent = 0);
    explicit SimpleServer(const QHostAddress &address, quint16 port, QObject *parent = 0);
    ~SimpleServer();
    bool run(const QHostAddress &address = QHostAddress::AnyIPv4, quint16 port = 38383);
    void stop();
    bool isRunning() const;
    void clearConnections();
    QByteArray passPhrase() const;
    void setPassPhrase(const QByteArray &key);
    QByteArray identityHint() const;
    void setIdentityHint(const QByteArray &identityHint);

signals:
//    void serverStatusChanged(bool isRun);
    void passPhraseChanged(const QByteArray &key);
    void identityHintChanged(const QByteArray &identityHint);

public slots:
protected slots:
//    void newConnect();
    void onDeliveredData(VectorInt32 vector, QByteArray data);
    void onSocketDestroyed();
    void removeConnection(ClientSocket *pSocket);
    void onUdpReadyRead();
    void onPreSharedKeyAuthenticationRequired(QSslPreSharedKeyAuthenticator *authenticator);
    void onSslError(const QList<QSslError> &errorList);
protected:
    virtual void incomingConnection(qintptr socketDescriptor);
    void udpInit(const QHostAddress &address = QHostAddress::AnyIPv4, quint16 port = 38383);
    void udpDestroy();

private:
    CommandProcessor m_processor;
    QUdpSocket *m_udpSocket {Q_NULLPTR};
    QSet<ClientSocket *> m_connections;
    QByteArray m_passPhrase;
    QByteArray m_identityHint;
    ProtocolVersion m_protocol {ProtocolVersion::ProtocolUnknown};
};

#endif // SIMPLESERVER_H
