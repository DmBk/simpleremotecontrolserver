#include "simpleserver.h"

#include <QSslConfiguration>
#include <QSslCipher>
#include <QDebug>
#include <QTime>
#include <QByteArray>
#include <QByteArrayList>


SimpleServer::SimpleServer(QObject *parent) : QTcpServer(parent)
{

}

SimpleServer::SimpleServer(const QHostAddress &address, quint16 port, QObject *parent) : SimpleServer(parent)
{
    this->run(address, port);
}

SimpleServer::~SimpleServer()
{
    qDebug() << QTime::currentTime() << __FUNCTION__;
    stop();
}

bool SimpleServer::run(const QHostAddress &address, quint16 port)
{
    qDebug() << QTime::currentTime() << __FUNCTION__;
    if (!this->isListening()) {
        if (!this->listen(address, port)) {
            this->close();
            qDebug() << "server not running";
            return false;
        }

//        connect(this, &QTcpServer::newConnection, this, &SimpleServer::newConnect);
//        emit serverStatusChanged(true);
        udpInit(address, port);
        qDebug() << "server running succesfull";
    }

    return true;
}

void SimpleServer::stop()
{
    qDebug() << QTime::currentTime() << __FUNCTION__;
    if (this->isListening()) {
        this->close();
        udpDestroy();
        clearConnections();
//        qDebug() << __FUNCTION__ << "server stoped";
    }    
}

bool SimpleServer::isRunning() const
{
    qDebug() << QTime::currentTime() << __FUNCTION__;
    return this->isListening();
}

void SimpleServer::clearConnections()
{
    qDebug() << QTime::currentTime() << __FUNCTION__;

    QSet<ClientSocket *>::iterator it = m_connections.begin();
    for (; it != m_connections.end(); ++it) {
        qDebug() << "removeConnection in function " << __FUNCTION__ << *it;
        (*it)->disconnect(SIGNAL(disconnected()));
        delete *it;
    }
    m_connections.clear();
}

QByteArray SimpleServer::passPhrase() const
{
    return m_passPhrase;
}

void SimpleServer::setPassPhrase(const QByteArray &key)
{
    QSslConfiguration sslConfig = QSslConfiguration::defaultConfiguration();
    QList<QSslCipher> ciphersList;
    ciphersList << QSslCipher("PSK-AES256-CBC-SHA");
    sslConfig.setCiphers(ciphersList);
    QSslConfiguration::setDefaultConfiguration(sslConfig);

    if (key != m_passPhrase) {
        m_passPhrase = key;
        emit passPhraseChanged(m_passPhrase);
    }
}

QByteArray SimpleServer::identityHint() const
{
    return m_identityHint;
}

void SimpleServer::setIdentityHint(const QByteArray &identityHint)
{
    if (identityHint != m_identityHint) {
        m_identityHint = identityHint;
        emit identityHintChanged(m_identityHint);
    }
}

void SimpleServer::onDeliveredData(VectorInt32 vector, QByteArray data)
{
    qDebug() << QTime::currentTime() << __FUNCTION__ << sender() << vector << data;
    ClientSocket *pSocket = dynamic_cast<ClientSocket*>(sender());

    VectorInt32 replyVector;
    switch (pSocket->clientState()) {
    case ControlState::ReadyForUse :
//        qDebug() << QTime::currentTime() << "ControlState::ReadyForUse" << pSocket;
        pSocket->writeToSocket(m_processor.runCommand(vector, data));
        break;
    case ControlState::NotReady :
        if ((vector.size() > 1) && (vector[0] == static_cast<qint32>(Command::HandshakeRequest))) {
            if (vector[1] >= static_cast<qint32>(ProtocolVersion::ProtocolVersionMax)) {
                replyVector.push_back(static_cast<qint32>(Command::HandshakeAck));
                replyVector.push_back(static_cast<qint32>(ProtocolVersion::ProtocolVersionMax));
                m_protocol = ProtocolVersion::ProtocolVersionMax;
            } else if (vector[1] >= static_cast<qint32>(ProtocolVersion::ProtocolVersion_1)) {
                replyVector.push_back(static_cast<qint32>(Command::HandshakeAck));
                replyVector.push_back(vector[1]);
                m_protocol = static_cast<ProtocolVersion>(vector[1]);
            } else {
                replyVector.push_back(static_cast<qint32>(Command::Error));
                replyVector.push_back(static_cast<qint32>(ProtocolVersion::ProtocolUnknown));
                pSocket->writeToSocket(replyVector);
                break;
            }
        } else {
            replyVector.push_back(static_cast<qint32>(Command::Error));
            replyVector.push_back(static_cast<qint32>(ProtocolVersion::ProtocolUnknown));
            pSocket->writeToSocket(replyVector);
            break;
        }

        //if PreSharedKey empty connect as not encrypted socket
        if (!m_passPhrase.isEmpty()) {
            connect(pSocket, &ClientSocket::preSharedKeyAuthenticationRequired,
                    this, &SimpleServer::onPreSharedKeyAuthenticationRequired);
            connect(pSocket, SIGNAL(sslErrors(const QList<QSslError>&)), this, SLOT(onSslError(const QList<QSslError>&)));
            connect(pSocket, &QSslSocket::encrypted, [=](){ pSocket->setClientState(ControlState::ReadyForUse); });

            QSslConfiguration sslConfig(pSocket->sslConfiguration());
            sslConfig.setPreSharedKeyIdentityHint(m_identityHint);
            pSocket->setSslConfiguration(sslConfig);
            pSocket->writeToSocket(replyVector);
            pSocket->startServerEncryption();
            QTimer::singleShot(5000, [=](){ if (m_connections.contains(pSocket) && pSocket->clientState() != ControlState::ReadyForUse)
                                                pSocket->abort();
                                           });
            qDebug() << "Start Server Encryption";
        } else {
            pSocket->setClientState(ControlState::ReadyForUse);
            pSocket->writeToSocket(replyVector);
        }
        break;
    default:
        replyVector.push_back(static_cast<qint32>(Command::Error));
        pSocket->writeToSocket(replyVector);
        break;
    }
}

void SimpleServer::onSocketDestroyed()
{
    qDebug() << QTime::currentTime() << __FUNCTION__ << sender();
}

void SimpleServer::removeConnection(ClientSocket *pSocket)
{
    qDebug() << QTime::currentTime() << __FUNCTION__;

//    pSocket->disconnect(SIGNAL(disconnected()));
    pSocket->deleteLater();
    m_connections.remove(pSocket);
}

void SimpleServer::onUdpReadyRead()
{
    while (m_udpSocket->hasPendingDatagrams()) {
            QByteArray datagram;
            datagram.resize(m_udpSocket->pendingDatagramSize());
            QHostAddress senderAddress;
            quint16 senderPort;

            m_udpSocket->readDatagram(datagram.data(), datagram.size(),
                                    &senderAddress, &senderPort);
            qDebug() << datagram << datagram.size() << senderAddress.toString() << senderPort;
            QDataStream in(datagram);
            qint32 reply;
            in >> reply;
            if (reply == static_cast<qint32>(Command::ServerDiscover)) {
                datagram.clear();
                QDataStream out(&datagram, QIODevice::WriteOnly);
                out << Command::ServerOffer;
                m_udpSocket->writeDatagram(datagram, senderAddress, senderPort);
            }
    }
}

void SimpleServer::onPreSharedKeyAuthenticationRequired(QSslPreSharedKeyAuthenticator *authenticator)
{
    qDebug() << QTime::currentTime() << __FUNCTION__ << sender();
    QByteArrayList dataList;
    dataList << m_passPhrase << authenticator->identityHint() << authenticator->identity();
    authenticator->setPreSharedKey(deriveKey(dataList, m_protocol));
    qDebug() << "authenticator->identity() " << authenticator->identity() <<
                "authenticator->identityHint() " << authenticator->identityHint() <<
                "authenticator->preSharedKey() " << authenticator->preSharedKey() <<
                "authenticator " << authenticator ;
//    ClientSocket *pDebugSocket = dynamic_cast<ClientSocket*>(sender());
//    qDebug() << "sender()->mode() " << pDebugSocket->mode() <<
//                "sender()->sslConfiguration().preSharedKeyIdentityHint()" << pDebugSocket->sslConfiguration().preSharedKeyIdentityHint();
}

void SimpleServer::onSslError(const QList<QSslError> &errorList)
{
    qDebug() << QTime::currentTime() << __FUNCTION__;
    qDebug() << errorList;
}

void SimpleServer::incomingConnection(qintptr socketDescriptor)
{
    qDebug() << QTime::currentTime() << __FUNCTION__ << sender();
    ClientSocket *pSocket = new ClientSocket(this);

    if (!pSocket->setSocketDescriptor(socketDescriptor)) {
        qDebug() << QString("!pSocket->setSocketDescriptor(%1))").arg(socketDescriptor);
        pSocket->deleteLater();
        return;
    }

    //needed if not use deleteLater for socket
//    this->addPendingConnection(pSocket);

    m_connections << pSocket;

    pSocket->setReadYourself();
//    connect(pSocket, &QSslSocket::disconnected, pSocket, &QSslSocket::deleteLater);
    connect(pSocket, &QSslSocket::disconnected, [=](){ this->removeConnection(pSocket);});
//    connect(pSocket, &QSslSocket::destroyed, this, &SimpleServer::onSocketDestroyed);
//    connect(pSocket, &QSslSocket::readyRead, [=](){ this->readFromSocket(pSocket); });
    connect(pSocket, &ClientSocket::dataReaded, this, &SimpleServer::onDeliveredData);    
//    connect(pSocket, &QSslSocket::sslErrors, this, &SimpleServer::onSslError);    
}

void SimpleServer::udpInit(const QHostAddress &address, quint16 port)
{
    if (m_udpSocket != Q_NULLPTR) {
        udpDestroy();
    }

    m_udpSocket = new QUdpSocket();
    m_udpSocket->bind(address, port, QUdpSocket::ShareAddress);
    connect(m_udpSocket, &QAbstractSocket::readyRead, this, &SimpleServer::onUdpReadyRead);
}

void SimpleServer::udpDestroy()
{
    if (m_udpSocket != Q_NULLPTR) {
        m_udpSocket->abort();
        disconnect(m_udpSocket, &QAbstractSocket::readyRead, this, &SimpleServer::onUdpReadyRead);
        delete m_udpSocket;
        m_udpSocket = Q_NULLPTR;
    }
}
