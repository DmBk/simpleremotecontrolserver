#include "commandprocessor.h"
#include <QDebug>
#include <QTime>

CommandProcessor::CommandProcessor(QObject *parent) : QObject(parent), m_popup(Q_NULLPTR)
{
    connect(&m_timer, &QTimer::timeout, this, &CommandProcessor::onTimer);
}

CommandProcessor::~CommandProcessor()
{
    delete m_popup;
}

VectorInt32 CommandProcessor::runCommand(const VectorInt32 &commandVector, const QByteArray &parametrArray)
{
    VectorInt32 result {commandVector};
    qDebug() << QTime::currentTime() << __FUNCTION__ << commandVector.size() << commandVector[0];
    if (commandVector.size() < 1) {
        result.push_front(Command::CommandNeedMoreParametrs);
        return result;
    }

    HWND hWnd = FindWindow(L"Shell_TrayWnd", NULL);
    LRESULT lResult {LB_OKAY};

    switch (static_cast<Command::Commands>(commandVector[0])) {
    case Command::MediaVolumeDown:
        lResult = SendMessage(hWnd, WM_APPCOMMAND, 0, MAKELPARAM(0,APPCOMMAND_VOLUME_DOWN));
//        commandDone(lResult != LB_ERR, commandVector, parametrArray);
        break;
    case Command::MediaVolumeMute:
        lResult = SendMessage(hWnd, WM_APPCOMMAND, 0, MAKELPARAM(0,APPCOMMAND_VOLUME_MUTE));
//        commandDone(lResult != LB_ERR, commandVector, parametrArray);
        break;
    case Command::MediaVolumeUp:
        lResult = SendMessage(hWnd, WM_APPCOMMAND, 0, MAKELPARAM(0,APPCOMMAND_VOLUME_UP));
//        commandDone(lResult != LB_ERR, commandVector, parametrArray);
        break;
    case Command::MediaPlay:
        lResult = SendMessage(HWND_BROADCAST, WM_APPCOMMAND, 0, MAKELPARAM(0,APPCOMMAND_MEDIA_PLAY));
//        commandDone(lResult != LB_ERR, commandVector, parametrArray);
        break;
    case Command::MediaStop:
        lResult = SendMessage(HWND_BROADCAST, WM_APPCOMMAND, 0, MAKELPARAM(0,APPCOMMAND_MEDIA_STOP));
//        commandDone(lResult != LB_ERR, commandVector, parametrArray);
        break;
    case Command::MediaPrevious:
        lResult = SendMessage(HWND_BROADCAST, WM_APPCOMMAND, 0, MAKELPARAM(0,APPCOMMAND_MEDIA_PREVIOUSTRACK));
//        commandDone(lResult != LB_ERR, commandVector, parametrArray);
        break;
    case Command::MediaNext:
        lResult = SendMessage(HWND_BROADCAST, WM_APPCOMMAND, 0, MAKELPARAM(0,APPCOMMAND_MEDIA_NEXTTRACK));
//        commandDone(lResult != LB_ERR, commandVector, parametrArray);
        break;
    case Command::MediaPlayPause:
        lResult = SendMessage(HWND_BROADCAST, WM_APPCOMMAND, 0, MAKELPARAM(0,APPCOMMAND_MEDIA_PLAY_PAUSE));
//        commandDone(lResult != LB_ERR, commandVector, parametrArray);
        break;
    case Command::PowerLogoff:        
    case Command::PowerOff:        
    case Command::PowerReboot:
        shutdownSystem(static_cast<Command::Commands>(commandVector[0]));
        qDebug() << "Power" << commandVector[0];
        break;
    case Command::PowerLockScreen:
        LockWorkStation();
        break;
    case Command::PowerSleep:
        suspendSystem(true);
    case Command::PowerHibernate:
        suspendSystem(false);
    case Command::TimerAdd:
        if (!addTimer(commandVector, parametrArray))
            lResult = LB_ERR;
        break;
    case Command::TimerAbort:
        m_timer.stop();
        if (m_popup == Q_NULLPTR) {
            qDebug() << "m_popup == Q_NULLPTR";
            m_popup = new PopupWindow();
        }
        m_popup->setPopupText(tr("Таймер отключен"));
        m_popup->show();
        break;
    default:
        result.push_front(Command::CommandNotFound);
        return result;
        break;
    }

    if (lResult != LB_ERR) {
        result.push_front(Command::CommandDone);
        return result;
    }

    result.push_front(Command::CommandFail);
    return result;
}

bool CommandProcessor::addTimer(VectorInt32 vector, QByteArray parametrArray)
{
    qDebug() << QTime::currentTime() << __FUNCTION__ ;
    if (vector.size() < 3 || vector[0] != static_cast<qint32>(Command::TimerAdd))
        return false;

    m_savedVector.clear();
    const qint32 DelayForLastPopup {60000};
    qint32 delay {0};
    QString messages;
    m_savedArray = std::move(parametrArray);
    if (vector[1] > DelayForLastPopup) {
        delay = vector[1] - DelayForLastPopup;
        vector[1] = DelayForLastPopup;
        m_savedVector = std::move(vector);
        m_timer.start(delay);
        messages.setNum((delay + m_savedVector[1]) / 1000 / 60);
    } else {
        m_savedVector = std::move(vector.mid(2));
        m_timer.start(vector[1]);
        messages.setNum(vector[1] / 1000 / 60);
    }

    messages = QString(m_savedArray) + " " + trUtf8("через") + " " + messages + " " +
            (messages.toInt() == 1 ? tr("минуту") :
                                     (messages.toInt() > 4 ? tr("минут") : tr("минуты")));
    if (m_popup == Q_NULLPTR) {
        qDebug() << "m_popup == Q_NULLPTR";
        m_popup = new PopupWindow();
    }
    m_popup->setPopupText(messages);
    m_popup->show((delay == 0 ? DelayForLastPopup : 5000));

    return true;
}

void CommandProcessor::onTimer()
{
    qDebug() << QTime::currentTime() << __FUNCTION__ ;
    m_timer.stop();
    runCommand(m_savedVector, m_savedArray);
}

bool CommandProcessor::shutdownSystem(Command::Commands commandId)
{
#ifdef Q_OS_WIN32
    UINT flag;
    switch (commandId) {
    case Command::PowerReboot:
        flag = EWX_REBOOT;
        break;
    case Command::PowerOff:
        flag = EWX_POWEROFF;
        break;
    case Command::PowerLogoff:
        flag = EWX_LOGOFF;
        break;
    default:
        flag = EWX_POWEROFF;
        break;
    }
    //https://msdn.microsoft.com/en-us/library/windows/desktop/aa376871(v=vs.85).aspx
    HANDLE hToken;
    TOKEN_PRIVILEGES tkp;

    // Get a token for this process.

    if (!OpenProcessToken(GetCurrentProcess(),
                          TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken))
        return( FALSE );

    // Get the LUID for the shutdown privilege.

    LookupPrivilegeValue(NULL, SE_SHUTDOWN_NAME,
                         &tkp.Privileges[0].Luid);

    tkp.PrivilegeCount = 1;  // one privilege to set
    tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

    // Get the shutdown privilege for this process.

    AdjustTokenPrivileges(hToken, FALSE, &tkp, 0,
                          (PTOKEN_PRIVILEGES)NULL, 0);

    if (GetLastError() != ERROR_SUCCESS)
        return FALSE;

    // Shut down the system and force all applications to close.

    if (!ExitWindowsEx(flag, 0))
        return FALSE;

    //shutdown was successful
    return TRUE;
#endif

    //return FALSE if OS not Windows
    return FALSE;
}

bool CommandProcessor::suspendSystem(bool suspend)
{
#ifdef Q_OS_WIN32
    //https://msdn.microsoft.com/en-us/library/windows/desktop/aa376871(v=vs.85).aspx
    HANDLE hToken;
    TOKEN_PRIVILEGES tkp;

    // Get a token for this process.

    if (!OpenProcessToken(GetCurrentProcess(),
                          TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken))
        return( FALSE );

    // Get the LUID for the shutdown privilege.

    LookupPrivilegeValue(NULL, SE_SHUTDOWN_NAME,
                         &tkp.Privileges[0].Luid);

    tkp.PrivilegeCount = 1;  // one privilege to set
    tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

    // Get the shutdown privilege for this process.

    AdjustTokenPrivileges(hToken, FALSE, &tkp, 0,
                          (PTOKEN_PRIVILEGES)NULL, 0);

    if (GetLastError() != ERROR_SUCCESS)
        return FALSE;

    if(!SetSystemPowerState(suspend, FALSE))
        return FALSE;

    return TRUE;
#endif
}
