#include "popupwindow.h"
#include <QPainter>
#include <QApplication>
#include <QDesktopWidget>
#include <QDebug>

PopupWindow::PopupWindow(QWidget *parent) : QWidget(parent)
{
    setWindowFlags(Qt::FramelessWindowHint |        // Отключаем оформление окна
                   Qt::Tool |                       // Отменяем показ в качестве отдельного окна
                   Qt::WindowStaysOnTopHint);       // Устанавливаем поверх всех окон
    setAttribute(Qt::WA_TranslucentBackground);     // Указываем, что фон будет прозрачным
    setAttribute(Qt::WA_ShowWithoutActivating);     // При показе, виджет не получается фокуса автоматически

    m_animation.setTargetObject(this);                // Устанавливаем целевой объект анимации
    m_animation.setPropertyName("popupOpacity");      // Устанавливаем анимируемое свойство
    connect(&m_animation, &QAbstractAnimation::finished, this, &PopupWindow::hide); /* Подключаем сигнал окончания
                                                                                     * анимации к слоты скрытия
                                                                                     * */
    // Настройка текста уведомления
    m_label.setAlignment(Qt::AlignHCenter | Qt::AlignVCenter); // Устанавливаем по центру
    // И настраиваем стили
    m_label.setStyleSheet("QLabel { color : white; "
                        "margin-top: 6px;"
                        "margin-bottom: 6px;"
                        "margin-left: 10px;"
                        "margin-right: 10px;"
                        "font-size: 22pt}");

    // Производим установку текста в размещение, ...
    m_layout.addWidget(&m_label, 0, 0);
    setLayout(&m_layout); // которое помещаем в виджет

    // По сигналу таймера будет произведено скрытие уведомления, если оно видимо
    m_timer = new QTimer();
    connect(m_timer, &QTimer::timeout, this, &PopupWindow::hideAnimation);
}

PopupWindow::~PopupWindow()
{
    qDebug() << __FUNCTION__;
    delete m_timer;
}

void PopupWindow::setPopupOpacity(float opacity)
{
    m_popupOpacity = opacity;
    setWindowOpacity(opacity);
}

float PopupWindow::getPopupOpacity() const
{
    return m_popupOpacity;
}

void PopupWindow::setPopupText(const QString &text)
{
    m_label.setText(text);    // Устанавливаем текст в Label
    adjustSize();           // С пересчётом размеров уведомления
}

void PopupWindow::show(int delayShow)
{
    qDebug() << __FUNCTION__;
    setGeometry(QApplication::desktop()->availableGeometry().width() - 36 - width() + QApplication::desktop() -> availableGeometry().x(),
                    QApplication::desktop()->availableGeometry().height() - 36 - height() + QApplication::desktop() -> availableGeometry().y(),
                    width(),
                    height());
        if(!isVisible()){
            setWindowOpacity(0.0);

            m_animation.setDuration(150);
            m_animation.setStartValue(0.0);
            m_animation.setEndValue(1.0);

            QWidget::show();
            m_animation.start();
            m_timer->start(delayShow);
        } else {
            m_animation.setDuration(windowOpacity()*150);
            m_animation.setStartValue(windowOpacity());
            m_animation.setEndValue(1.0);
            m_animation.start();
            m_timer->start(delayShow);
    }
}

void PopupWindow::hideAnimation()
{
    m_timer->stop();                  // Останавливаем таймер
    m_animation.setDuration(1000);    // Настраиваем длительность анимации
    m_animation.setStartValue(1.0);   // Стартовое значение будет 1 (полностью непрозрачный виджет)
    m_animation.setEndValue(0.0);     // Конечное - полностью прозрачный виджет
    m_animation.start();              // И запускаем анимацию
}

void PopupWindow::hide()
{
    // Если виджет прозрачный, то скрываем его
    if(getPopupOpacity() == 0.0){
        QWidget::hide();
    }
}

void PopupWindow::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event)

    /* А теперь настраиваем фон уведомления,
     * который является прямоугольником с чёрным фоном
     * */
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing); // Включаем сглаживание

    // Подготавливаем фон. rect() возвращает внутреннюю геометрию виджета уведомления, по содержимому
    QRect roundedRect;
    roundedRect.setX(rect().x() + 5);
    roundedRect.setY(rect().y() + 5);
    roundedRect.setWidth(rect().width() - 10);
    roundedRect.setHeight(rect().height() - 10);

    // Кисть настраиваем на чёрный цвет в режиме полупрозрачности 180 из 255
    painter.setBrush(QBrush(QColor(0,0,0,180)));
    painter.setPen(Qt::NoPen); // Край уведомления не будет выделен

    // Отрисовываем фон с закруглением краёв в 10px
    painter.drawRoundedRect(roundedRect, 10, 10);
}


