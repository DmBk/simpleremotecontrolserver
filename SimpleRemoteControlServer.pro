#-------------------------------------------------
#
# Project created by QtCreator 2016-07-10T16:54:04
#
#-------------------------------------------------

QT       += core gui widgets network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SimpleRCServer
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    simpleserver.cpp \
    commandprocessor.cpp \
    popupwindow.cpp \
    ../simpleremotecontrol/clientsocket.cpp

HEADERS  += mainwindow.h \
    simpleserver.h \
    commandprocessor.h \
    popupwindow.h \
    ../simpleremotecontrol/clientsocket.h

FORMS    += mainwindow.ui

win32-msvc*{
    LIBS += "C:\Program Files (x86)\Windows Kits\10\Lib\10.0.14393.0\um\x86\User32.Lib"
    LIBS += "C:\Program Files (x86)\Windows Kits\10\Lib\10.0.14393.0\um\x86\advapi32.Lib"
#for SetSystemPowerState
    LIBS += "C:\Program Files (x86)\Windows Kits\10\Lib\10.0.14393.0\um\x86\kernel32.lib"
    INCLUDEPATH += "c:\Program Files (x86)\Windows Kits\10\Include\10.0.14393.0\um" 
    message("msvc")
}
win32-g++ {
   message("mingw"))
}

RESOURCES += \
    resources.qrc
