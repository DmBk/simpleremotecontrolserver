#ifndef COMMANDPROCESSOR_H
#define COMMANDPROCESSOR_H

#include <QObject>
#include "../simpleremotecontrol/src_protocol.h"
#include <QTimer>
#include <utility> //for std::move
#include "popupwindow.h"

#ifdef Q_OS_WIN32
    #include <Windows.h>
    #include <WinUser.h>
#endif

class CommandProcessor : public QObject
{
    Q_OBJECT
public:
    explicit CommandProcessor(QObject *parent = 0);
    ~CommandProcessor();
    VectorInt32 runCommand(const VectorInt32 &commandVector, const QByteArray &parametrArray = QByteArray());

signals:
//    void commandDone(bool success, const VectorInt32 &commandVector, const QByteArray &parametrArray);

public slots:
protected slots:
    void onTimer();
protected:
    //added new timer, not "const &" because can run width member m_savedVector and m_savedArray
    bool addTimer(VectorInt32 vector, QByteArray parametrArray);
private:
    bool shutdownSystem(Command::Commands commandId);
    bool suspendSystem(bool suspend);
    VectorInt32 m_savedVector;
    QByteArray m_savedArray;
    QTimer m_timer;
    PopupWindow *m_popup;
};

#endif // COMMANDPROCESSOR_H
