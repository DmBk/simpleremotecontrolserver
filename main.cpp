#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QCoreApplication::setApplicationName("Simple Remote Control Server");
    QCoreApplication::setOrganizationName("DmBk");

    QApplication a(argc, argv);
    MainWindow w;    
//    w.show();
    w.parseCommandLine(argc, argv);

    return a.exec();
}
