#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent), ui(new Ui::MainWindow), m_trayIcon(new QSystemTrayIcon(this)),
    m_server(new SimpleServer(this)), m_serviceAction(new QAction(tr("Запустить сервис"), this))
{
    ui->setupUi(this);
    this->setWindowIcon(QIcon(":/images/logo@2x.png"));
//    this->setWindowIcon(this->style()->standardIcon(QStyle::SP_ComputerIcon));
    this->setWindowTitle(QCoreApplication::applicationName());

//    m_trayIcon->setIcon(this->style()->standardIcon(QStyle::SP_ComputerIcon));
    m_trayIcon->setIcon(QIcon(":/images/logo@2x.png"));
    QMenu *menu = new QMenu(this);
    QAction *viewWindow = new QAction(tr("Развернуть окно"), this);
    QAction *quitAction = new QAction(tr("Выход"), this);

    connect(m_serviceAction, &QAction::triggered, this, &MainWindow::startService);
    connect(viewWindow, SIGNAL(triggered()), this, SLOT(show()));
    connect(quitAction, SIGNAL(triggered()), this, SLOT(close()));
    menu->addAction(m_serviceAction);
    menu->addAction(viewWindow);
    menu->addAction(quitAction);
    m_trayIcon->setContextMenu(menu);
    m_trayIcon->show();
    m_trayIcon->setToolTip(tr("SimpleRemoteServer") + "\n" + tr("Сервер остановлен"));

    readSettings();

    connect(m_trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
            this, SLOT(iconActivated(QSystemTrayIcon::ActivationReason)));
    connect(ui->startServiceButton, SIGNAL(clicked(bool)), this, SLOT(startService()));
    connect(ui->stopServiceButton, &QPushButton::clicked, this, &MainWindow::stopService);
    connect(ui->exitButton, &QPushButton::clicked, this, &MainWindow::close);
    connect(ui->pskCheckBox, &QCheckBox::clicked, this, &MainWindow::saveSettings);
    connect(ui->pskLineEdit, &QLineEdit::editingFinished, this, &MainWindow::saveSettings);
    connect(ui->autostartCheckBox, &QCheckBox::clicked, this, &MainWindow::saveSettings);
    connect(ui->closeCheckBox, &QCheckBox::clicked, this, &MainWindow::saveSettings);
    connect(ui->minimizeCheckBox, &QCheckBox::clicked, this, &MainWindow::saveSettings);
    connect(m_server, &SimpleServer::passPhraseChanged, this, &MainWindow::onPreSharedKeyChanged);

    m_server->setIdentityHint(QByteArray("SimpleRemoteControlServer"));

    qDebug() << QCoreApplication::applicationName() << "path" << QCoreApplication::applicationFilePath();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::parseCommandLine(int argc, char *argv[])
{
    bool minimized = false;
    bool exit = false;
    for (int i = 1; i < argc; ++i) {
        if (QString("--minimize") == argv[i]) {
            if (ui->minimizeCheckBox->isChecked()) {
                hide();
            } else {
                showMinimized();
            }
            minimized = true;
        } else if (QString("--start") == argv[i]) {
            startService();
        } else if (QString("--clear") == argv[i]) {
            clearSettings();
        } else if (QString("--exit") == argv[i]) {
            exit = true;
        } else {
            qDebug() << "Unknown parametr in command line:" << argv[i];
        }
    }

    if (exit) {
//        setVisible();
        this->close();
    }
    if (!minimized)
        show();
}

void MainWindow::hideEvent(QHideEvent *event)
{
    QMainWindow::hideEvent(event);
    if (ui->minimizeCheckBox->checkState() == Qt::Checked)
        this->hide();
//    qDebug() << __FUNCTION__;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
//    qDebug() << __FUNCTION__ << event->type();
    //При закрытии из контекстного меню в трее окно не будет активно и приложение закроется
    if ((ui->closeCheckBox->isChecked()) && this->isActiveWindow()) {
        event->ignore();
        this->hide();
        m_trayIcon->showMessage(QCoreApplication::applicationName(),
                                tr("Приложение свернуто в трей и продолжает работать."),
                                QSystemTrayIcon::MessageIcon(QSystemTrayIcon::Information),
                                2000);
    }
}

void MainWindow::readSettings()
{
#ifdef Q_OS_WIN32
    QSettings autostartSettings("HKEY_CURRENT_USER\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run",
                                QSettings::NativeFormat);
    ui->autostartCheckBox->setChecked(autostartSettings.contains(QCoreApplication::applicationName())
                                      && autostartSettings.value(QCoreApplication::applicationName(), QString()).
                                      toString().contains(QDir::toNativeSeparators(QCoreApplication::applicationFilePath())));
#endif

    QSettings settings(QCoreApplication::organizationName(), QCoreApplication::applicationName());
    ui->pskCheckBox->setChecked(settings.value("PskEnabled", false).toBool());
    ui->pskLineEdit->setText(QString::fromUtf8(QByteArray::fromBase64(settings.value("Psk").toByteArray())));
    ui->closeCheckBox->setChecked(settings.value("CloseToTray", false).toBool());
    ui->minimizeCheckBox->setChecked(settings.value("MinimizeToTray", true).toBool());

    if (ui->pskCheckBox->isChecked()) {
        ui->pskLineEdit->setEnabled(true);
        m_server->setPassPhrase(ui->pskLineEdit->text().toUtf8());
    }

    qDebug() <<__FUNCTION__ << settings.value("CloseToTray", false).toBool()
            << settings.value("MinimizeToTray", true).toBool();
}

QStringList MainWindow::listenIp4Address() const
{
    QList<QHostAddress> ipAddressesList = QNetworkInterface::allAddresses();
    QStringList ipAddrStringList;
    QPair<QHostAddress, int> linkLocalSubnet {qMakePair(QHostAddress("169.254.0.0"), 16)};
    for (int i = 0; i < ipAddressesList.size(); ++i) {
            if (ipAddressesList.at(i) != QHostAddress::LocalHost &&
                    ipAddressesList.at(i).toIPv4Address() &&
                    !ipAddressesList.at((i)).isInSubnet(linkLocalSubnet)) {
                ipAddrStringList << ipAddressesList.at(i).toString();
            }
        }

    return ipAddrStringList;
}

void MainWindow::clearSettings()
{
    ui->autostartCheckBox->setChecked(false);
    QSettings settings(QCoreApplication::organizationName(), QCoreApplication::applicationName());
    if (settings.contains(QCoreApplication::applicationName())) {
        settings.remove(QCoreApplication::applicationName());
        settings.sync();
    }
}

void MainWindow::iconActivated(QSystemTrayIcon::ActivationReason reason)
{
//    qDebug() << __FUNCTION__;
    if (reason == QSystemTrayIcon::DoubleClick) {
        if(!this->isVisible()){
            this->show();
            if (this->isMinimized()) {
                this->showNormal();
                this->raise();
                this->activateWindow();
            }
        } else {
            this->hide();
            m_trayIcon->showMessage(QCoreApplication::applicationName(),
                                    tr("Приложение свернуто в трей и продолжает работать."),
                                    QSystemTrayIcon::MessageIcon(QSystemTrayIcon::Information),
                                    2000);
        }
    }
}

void MainWindow::startService()
{
    qDebug() << __FUNCTION__;
    if (m_server->run()) {
        QPalette labelPalette = ui->labelStatusService->palette();
        labelPalette.setColor(QPalette::WindowText, Qt::darkGreen);
        ui->labelStatusService->setPalette(labelPalette);
        ui->labelStatusService->setText(tr("Сервис запущен"));
        m_trayIcon->setToolTip(tr("SimpleRemoteServer" "\n"
                                      "Сервер работает"));
        m_serviceAction->setText(tr("Остановить сервис"));
        disconnect(m_serviceAction, &QAction::triggered, this, &MainWindow::startService);
        connect(m_serviceAction, &QAction::triggered, this, &MainWindow::stopService);
        if(m_listenIpAddrModel == Q_NULLPTR) {
            m_listenIpAddrModel = new QStringListModel(this);
            ui->listViewIpAddr->setModel(m_listenIpAddrModel);
        }
        m_listenIpAddrModel->setStringList(listenIp4Address());

    } else {
        QPalette labelPalette = ui->labelStatusService->palette();
        labelPalette.setColor(QPalette::WindowText, Qt::red);
        ui->labelStatusService->setPalette(labelPalette);
        ui->labelStatusService->setText(tr("Сервис не запущен"));
        if(m_listenIpAddrModel != Q_NULLPTR) {
            m_listenIpAddrModel->setStringList(QStringList());
        }
    }
}

void MainWindow::stopService()
{
    m_server->stop();
    QPalette labelPalette = ui->labelStatusService->palette();
    labelPalette.setColor(QPalette::WindowText, Qt::red);
    ui->labelStatusService->setPalette(labelPalette);
    ui->labelStatusService->setText(tr("Сервис остановлен"));
    if(m_listenIpAddrModel != Q_NULLPTR) {
        m_listenIpAddrModel->setStringList(QStringList());
    }
    m_trayIcon->setToolTip(tr("SimpleRemoteServer" "\n"
                                  "Сервер остановлен"));
    m_serviceAction->setText(tr("Запустить сервис"));
    disconnect(m_serviceAction, &QAction::triggered, this, &MainWindow::stopService);
    connect(m_serviceAction, &QAction::triggered, this, &MainWindow::startService);
}

void MainWindow::saveSettings()
{
#ifdef Q_OS_WIN32
    QSettings autostartSettings("HKEY_CURRENT_USER\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run",
                                QSettings::NativeFormat);
    if (ui->autostartCheckBox->isChecked()) {
        autostartSettings.setValue(QCoreApplication::applicationName(),
                                   "\"" + QDir::toNativeSeparators(QCoreApplication::applicationFilePath()) + "\" --minimize --start");
        qDebug() << "\"" + QDir::toNativeSeparators(QCoreApplication::applicationFilePath()) + "\" --minimize --start";
    } else {
        if (autostartSettings.contains(QCoreApplication::applicationName()))
            autostartSettings.remove(QCoreApplication::applicationName());
    }
    autostartSettings.sync();
#endif

    ui->pskLineEdit->setEnabled(ui->pskCheckBox->isChecked());
    if (ui->pskCheckBox->isChecked()) {
        m_server->setPassPhrase(ui->pskLineEdit->text().toUtf8());
    } else {
        m_server->setPassPhrase(QByteArray());
    }

    QSettings settings(QCoreApplication::organizationName(), QCoreApplication::applicationName());
    settings.setValue("PskEnabled", ui->pskCheckBox->isChecked());
    settings.setValue("Psk", ui->pskLineEdit->text().toUtf8().toBase64());
    settings.setValue("CloseToTray", ui->closeCheckBox->isChecked());
    settings.setValue("MinimizeToTray", ui->minimizeCheckBox->isChecked());
    settings.sync();
}

void MainWindow::onPreSharedKeyChanged()
{
    if (m_server->isRunning()) {
        stopService();
        startService();
        qDebug() << "Service restart";
    }
}

