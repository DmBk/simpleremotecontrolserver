#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSystemTrayIcon>
#include <QCloseEvent>
#include <QAction>
#include "simpleserver.h"
#include <QSettings>
#include <QDir>
#include <QIcon>
#include <QStringListModel>
#include <QNetworkInterface>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);    
    ~MainWindow();
    void parseCommandLine(int argc, char *argv[]);    

protected:
    virtual void hideEvent(QHideEvent *event);
    virtual void closeEvent(QCloseEvent * event);
    void readSettings();
    QStringList listenIp4Address() const;
    void clearSettings();

protected slots:
    void iconActivated(QSystemTrayIcon::ActivationReason reason);    
    void startService();
    void stopService();
    void saveSettings();
    void onPreSharedKeyChanged();

private:
    Ui::MainWindow *ui;
    QSystemTrayIcon *m_trayIcon;
    SimpleServer *m_server;
    QAction *m_serviceAction;
    QStringListModel *m_listenIpAddrModel {Q_NULLPTR};

};

#endif // MAINWINDOW_H
