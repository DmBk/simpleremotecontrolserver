#ifndef POPUPWINDOW_H
#define POPUPWINDOW_H

//http://www.evileg.ru/baza-znanij/qt/sozdayom-vsplyvayushhie-popup-soobshhenie-v-stile-gnome.html

#include <QWidget>
#include <QLabel>
#include <QGridLayout>
#include <QPropertyAnimation>
#include <QTimer>

class PopupWindow : public QWidget
{
    Q_OBJECT

    // Свойство полупрозрачности
    Q_PROPERTY(float popupOpacity READ getPopupOpacity WRITE setPopupOpacity)

public:
    explicit PopupWindow(QWidget *parent = 0);
    ~PopupWindow();
    void setPopupOpacity(float opacity);
    float getPopupOpacity() const;

signals:

public slots:
    void setPopupText(const QString& text); // Установка текста в уведомление
    void show(int delayShow = 5000);                            /* Собственный метод показа виджета
                                             * Необходимо для преварительной настройки анимации
                                             * */
protected slots:
    void hideAnimation();                   // Слот для запуска анимации скрытия
    void hide();                            /* По окончании анимации, в данном слоте делается проверка,
                                             * виден ли виджет, или его необходимо скрыть
                                             * */
protected:
    void paintEvent(QPaintEvent *event);    // Фон будет отрисовываться через метод перерисовки

private:
    QLabel m_label;           // Label с сообщением
    QGridLayout m_layout;     // Размещение для лейбла
    QPropertyAnimation m_animation;   // Свойство анимации для всплывающего сообщения
    float m_popupOpacity;     // Свойства полупрозрачности виджета
    QTimer *m_timer;          // Таймер, по которому виджет будет скрыт
};

#endif // POPUPWINDOW_H
